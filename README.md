Ring UKRAINE - System Enineering team

# Portable Power Monitor 

REQUIREMENTS AND INSTALLATION



| Processor | Intel i5 equivalent or higher recommended |
| RAM | Min 4 GB (200MB+300MB/h)|
| Screen Resolution | 1024 x 768 pixels |
| OS | Windows 10/8.1/8/7 SP1 (64-bit) |
| Disk Space | 620 MB |

### Installation Process

To install PPM software you need to Run Install\_PPM.exe this installer includes LabView Run-Time Engine, NI MAX, NI VISA and program itself.

If you need to install some different version run installer of newer version of program and installer will choose what to update automatically. Or you can go to system settings Apps&amp;Features find National Instruments Software click uninstall. NI packages manager will be opened than you need to choose all packages and click uninstall.

# Using The PPM Software

After Hardware installation part is completed and device has right firmware. You can start to use PPM software application.

To open program click Start and type &quot;PPM&quot; if installation was successful than application should appear in search. Run application.

Program is separated to two parts: Data Output and Tools Display.

**Data Output window** is used for viewing measurements in two possible formats : Graph and Table.

**App control window** needed for application and measurement device control.

After opening program does nothing it is in Idle state which means that samples are not reading from device and com port has no active session opened.

### System tab

What **System Tab** does, it controls device connection, internal data buffer where data from com port is stored and data representation settings. If you have Device connected to your PC and VISA driver is installed correctly you can click on drop down list that called **COM PORT** , click refresh and then next time your device must be discovered in that drop-down list. Choose device and click **Open** button to open com port session, also it can be closed by **Close** button.

After connection was established data that was fetched and parsed is saving in internal RAW data buffer which is limited by your RAM and if buffer is full you need to clear it by button **Clear Internal Buffer** or newly acquired data will be lost. Average modern PC have at least 8 GB of RAM so with this you can last around 16 hours (need more testing on that).  Quantity of samples in internal buffer can be seen on **Internal Buffer Length** indicator.

But if you need longer working period you have options:

- --Save data for measured period and clear data buffer (without stopping measurements). To do this you need to click on **Clear Internal Buffer** button.
- --Start measurement with with bigger averaging window Commands tab -\&gt;   **Window Length** control and set Sampling speed **Sampling Speed, ms/sample** control to longer time.
- --Take machine with more RAM or install more to existent.

As data quantity grow there will be need to represent only one part of measured data or give wide picture of raw data buffer. Wide range of measurements can be output only with down sampled data.

Control **UI Resolution** control decimation factor. There are settings for Faster update = 10k points and High resolution = 100k points. Which do you need is depends on situation.

Actual decimation factor for displayed window can be seen on **UI Decimation Factor** indicator.

When you do critical tests you need to be sure that your measurements is in specs of your measurement device for this reason status bar exist it turns Red for appropriate channel when there are current or voltage overload happens.

_Note. For operator to pay attention on this event indicator is not return back by itself after event has occurred and to reset this indicator you need to click_ **Clear Status** _button._

### Commands tab

By default device has to give measurements immediately after port is opened. But if device was used before it can be in stop mode, please look on debug indicator if its red then device is in stop mode and you need to start it. You can do it in **Commands Tab** :

Command Tab is responsible for sending commands to device and saving RAW data buffer to comma separated text data file with limitation < 1 Million measurements (longer files can't be analyzed in excel).

**Start** command is responsible for acquisition start and it must be started along with averaging **window length** parameter.

As device connection channel can&#39;t be fully transfer ADC data so down sampling need to be used. Currently you can choose 1ms decimation (correct me if I&#39;m wrong) or moving average window length. This can be controlled by giving number to **Window Length** control: number 0 turn off averaging and numbers \&gt; 0 turn on averaging filter.

**Stop** command must stop measurement process and there must be red light on debug led after this.

Also, you can send **custom commands** to device if needed. Major case for this is calibration coefficients writing command which is written by default in text window below this button. (each number is responsible for channel coefficient from 1 to 8).

Apart from command sending there also **Data export** controls: **Export** and **File Export Path**. Export is performed to comma or Tab separated text file with header.

_Note that maximum length is 1 million rows if your buffer is longer pop up window will ask you to set smaller window length or decimate all buffer to \&lt;1M points. Also you can specify window period and decimate that points too._

_Note. If you need to open this file in excel without importing you need to be sure that in Region – Additional date, time &amp; regional settings – Region -Additional Settings.. – List Separator is set to comma &quot;,&quot;._

File path you can be typed with file and extension right into **File Export Path** : box or click &quot;…&quot; button after what typical windows dialog will be opened where you can choose file to rewrite or put name and extension of new file.

### UI control tab

Next Tools tab to describe is **UI Control** – this tab contains control of channels which will be displayed on Graph, displayed window controls and measurements units for 2nd row of channels.

You can control which plot will be **displayed on graph** by Channels control P stands for power V/I stands for Voltage/Current, chN number of measurement channel and () is measurement unit. Second row of channels can be converted to Current representation bu Voltage or Current control.

You may want to see some earliest data acquired or some measured part of buffer in greater details. This is controlled by **UI window position** button. As length of visible window need to be changed too this can be done with **UI Window Length** in **UI Window Units**.

_Note._ **Graph Tools palette** _(Data Output window) can perform operations only on_ **points that chosen** _by_ _UI Window Length \* UI Window units (UI control tab) and decimated by UI Decimation Factor (System tab)__!!_

### Cursors tab

**Statistic** parameters between **Cursors** can be found on Cursors tab.

Cursor positions can be controlled on Graph -Data Output window by mouse or number in cursors palette.

Which statistic parameter will be calculated on values between Cursors is chosen by **Calculation type** control. Also, quantity of samples between cursors is showing on Samples number indicator.

Every number in this indicator represent calculated value from samples between cursors. Because samples is constantly acquiring in run mode this measurement is can be more adequately used in stop mode or when sample is not acquired. Calculation is performed on all channels by default between cursors and second row of channels is can be represented as Current or Voltage.

### Data output window

In this section **Data** display will be discussed.

Main View here called **Cursors**. Its Graph with cursors and other controls.

X coordinate of graph shows number of sample 0..N captured to measurement buffer.

Y coordinate represent measurement number according to units that channel has, there are three of them (Watts, Volts and seconds).

After some quantity of samples measured when data in UI you can stop measuring and browse them in graph and measure parameters by cursors.

**Main controls** of graph view is :

**Mouse modes** – this toolset is changing what will be performed when you click on graph. Options are: in plasing order 1)move cursors mode 2)graph scaling tools 3) hand like move of graph.

For use of **scaling tool** you need to turn off autoscale in scaling control panel.

When you zoom in some part of graph you will be able to move through graph with **slider**.

If you zoom in some part of graph and need to put there cursors without zooming out you can directly put numbers in **cursor positions** menu.

Also there are menu which is **plot control** we can see plot legend there and if click with right button on some of legend unit plot menu is arising you can choose different representation options from there as color, line type, interpolation setting and others.

Other window for data representation is called **Data Table**
This menu is simple numeric representation of RAW Data buffer, export to csv file does same thing.